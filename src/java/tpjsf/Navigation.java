/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpjsf;

import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Thomas Kint
 */
@Named(value = "navigation")
@ApplicationScoped
public class Navigation {

    /**
     * Creates a new instance of Navigation
     */
    public Navigation() {
    }

    public String hello() {
        return "hello_v4?faces-redirect=true";
    }
}
