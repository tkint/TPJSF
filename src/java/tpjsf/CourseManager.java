/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpjsf;

/**
 *
 * @author Thomas Kint
 */
import java.util.ArrayList;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CourseManager {

    private List<Course> listeCourses;

    public CourseManager() {
        listeCourses = new ArrayList<Course>();
    }

    public List<Course> findCourses() {
        return listeCourses;
    }

    public Course findCourse(Integer n) {
        int i = 0;
        Course course = null;

        while (i < listeCourses.size() && course == null) {
            if (listeCourses.get(i).getId() == n) {
                course = listeCourses.get(i);
            }
            i++;
        }

        return course;
    }

    public Course saveCourse(Course c) {
        if (c.getId() == null) {
            c.setId(listeCourses.size());
            listeCourses.add(c);
        } else {
            Course course = findCourse(c.getId());
            course.setName(c.name);
            course.setLevel(c.level);
            course.setHours(c.hours);
            course.setDescription(c.description);
        }
        return c;
    }

    public void deleteCourse(Course c) {
        if (findCourse(c.getId()) != null) {
            listeCourses.remove(c);
        }
    }
}
