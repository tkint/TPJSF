package tpjsf;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class CompteurApplication implements Serializable {

    private int valeur = 0;

    /**
     * Creates a new instance of CompteurApplication
     */
    public CompteurApplication() {
    }

    public Integer getCompteur() {
        return ++valeur;
    }

    @PostConstruct
    void init() {
        System.err.println("Create " + this);
    }

    @PreDestroy
    void close() {
        System.err.println("Close " + this);
    }
}
