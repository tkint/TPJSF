package tpjsf;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean()
@ViewScoped
public class CompteurVue implements Serializable {

    @ManagedProperty("#{compteurApplication}")
    CompteurApplication compteurApplication;
    private int valeur = 0;

    /**
     * Creates a new instance of CompteurApplication
     */
    public CompteurVue() {
    }

    public Integer getCompteur() {
        return ++valeur;
    }

    @PostConstruct
    void init() {
        System.err.println("Create " + this);
    }

    @PreDestroy
    void close() {
        System.err.println("Close " + this);
    }

    public void setCompteurApplication(CompteurApplication compteurApplication) {
        this.compteurApplication = compteurApplication;
    }
}
