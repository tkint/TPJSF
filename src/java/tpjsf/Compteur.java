package tpjsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class Compteur {

    private Integer valeur = 1000;
    
    /**
     * Creates a new instance of Compteur
     */
    public Compteur() {
    }

    public String incremente() {
        valeur++;
        return null; // ne pas se déplacer
    }

    public Integer getValeur() {
        return valeur;
    }
}
